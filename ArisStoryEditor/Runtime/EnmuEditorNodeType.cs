using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArisStudio.StoryEditor
{
    public enum EditorNodeType
    {
        UNKNOWN,
        START,
        DEFAULT_DIALOG,
    }

    public class EditorNodeTypeString
    {
        public const string ORIGIN = "BaseNode";
        public const string START = "RootNode";
        public const string DEFAULT_DIALOG = "DefaultDialogNode";
    }
}
