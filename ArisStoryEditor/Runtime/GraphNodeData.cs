using ArisStudio.StoryEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GraphNodeData
{
    [HideInInspector]
    public string GUID;
    public string Name;
    public EditorNodeType NodeType;
    [HideInInspector]
    public string NodeTypeStr;
    public Rect Position;
}
