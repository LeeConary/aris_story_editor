using ArisStudio.StoryEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DefaultGraphNodeData : GraphNodeData
{
    public string DialogContent;
    public DefaultGraphNodeData(GraphNodeData data)
    {
        GUID = data.GUID;
        Name = data.Name;
        NodeType = data.NodeType;
        NodeTypeStr = data.NodeTypeStr;
        Position = data.Position;
    }
}
