using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using UnityEngine;

[Serializable]
public class GraphSaveContainer : ScriptableObject
{
    #region NodeSaveDatas saved by type
    public List<GraphNodeData> normalNodeDatas;
    public List<DefaultGraphNodeData> defualtNodeDatas;
    #endregion

    #region NodeLinkDatas
    public List<GraphNodeLinkData> graphNodeLinkDatas;
    #endregion

    public void Init()
    {
        normalNodeDatas = new List<GraphNodeData>();
        defualtNodeDatas = new List<DefaultGraphNodeData>();
        graphNodeLinkDatas = new List<GraphNodeLinkData>();
    }
}
