using ArisStudio.StoryEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GraphNodeLinkData
{
    public string OriginNodeGUID;
    public string OriginNodeOutputPortName;
    public string TargetNodeGUID;
}
