using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace ArisStudio.StoryEditor
{
    public static class ElementStyleUtitlity
    {
        public static VisualElement AddClasses(this VisualElement element, params string[] values)
        {
            foreach (var className in values)
            {
                element.AddToClassList(className);
            }
            return element;
        }

        public static VisualElement AddStyleSheets(this VisualElement element, bool loadFromResource, params string[] styleSheetNames)
        {
            foreach (string styleSheetName in styleSheetNames)
            {
                /*StyleSheet styleSheet = (StyleSheet)EditorGUIUtility.Load(styleSheetName)*/;
                StyleSheet styleSheet = LoadStyleSheet(styleSheetName, loadFromResource);
                element.styleSheets.Add(styleSheet);
            }

            return element;
        }

        static StyleSheet LoadStyleSheet(string path, bool loadFromResource)
        {
            StyleSheet sheet = loadFromResource ?
                Resources.Load<StyleSheet>(path) :
                EditorGUIUtility.Load(path) as StyleSheet;
            return sheet;
        }
    }
}
