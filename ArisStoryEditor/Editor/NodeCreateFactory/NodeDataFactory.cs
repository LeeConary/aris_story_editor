﻿using System.Collections;
using UnityEngine;

namespace ArisStudio.StoryEditor
{
    using UnityEditor.Experimental.GraphView;
    public class NodeDataFactory
    {
        public static DefaultGraphNodeData CreatDefultGraphNodeData(BaseNode node)
        {
            DefaultDialogNode realNode = node as DefaultDialogNode;
            return realNode.GetData() as DefaultGraphNodeData;
        }
        public static GraphNodeData CreatGraphNodeData(BaseNode node)
        {
            return node.GetData();
        }
    }
}