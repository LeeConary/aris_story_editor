using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace ArisStudio.StoryEditor
{
    using System;
    using System.Collections;
    using System.Linq;
    using UnityEditor.Experimental.GraphView;
    using UnityEngine.UIElements;

    public class GraphIOModule
    {
        public MainGraphEditorView graphEditorView;

        public static readonly string GraphSaveRootPath = "Assets/Resources";
        public static GraphIOModule Instance { get; private set; }
        public static GraphIOModule CreateInstance(MainGraphEditorView graphView)
        {
            Instance = new GraphIOModule { graphEditorView = graphView };
            return Instance;
        }

        List<BaseNode> loadedNodes;
        public GraphIOModule() 
        {
            loadedNodes = new List<BaseNode>();
        }
        public void SaveGraph(string fileName)
        {
            GraphSaveContainer graphSaveContainer = ScriptableObject.CreateInstance<GraphSaveContainer>();
            graphSaveContainer.Init();
            if (!AssetDatabase.IsValidFolder(GraphSaveRootPath))
                AssetDatabase.CreateFolder("Assets", "Resources");

            SaveNodeDatas(graphSaveContainer);
            SaveNodeLinkDatas(graphSaveContainer);

            AssetDatabase.CreateAsset(graphSaveContainer, Path.Combine(GraphSaveRootPath, $"{fileName}.aris.asset"));
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            // string savePath = EditorUtility.SaveFilePanel("Save Graph", Application.dataPath, $"{fileName}.aris", "asset");
            //if (string.IsNullOrEmpty(savePath))
            //{
            //    EditorUtility.DisplayDialog("Save File Error", "Save Canceled", "OK");
            //    return;
            //}
        }
        public void LoadGraph(string fileName)
        {
            GraphSaveContainer graphSaveContainer = Resources.Load<GraphSaveContainer>(fileName);
            graphEditorView.ClearAllNodesAndEdges();

            LoadNodeDatas(graphSaveContainer);
            LoadNodeLinkDatas(graphSaveContainer);
        }


        #region Private Functions - Save
        private void SaveNodeDatas(GraphSaveContainer graphSaveContainer)
        {
            List<BaseNode> nodes = graphEditorView.nodes.Cast<BaseNode>().ToList();
            for (int i = 0; i < nodes.Count; i++)
            {
                BaseNode tempNode = nodes[i];
                if (tempNode.NodeTypeStr == EditorNodeTypeString.DEFAULT_DIALOG)
                {
                    DefaultGraphNodeData nodeData = NodeDataFactory.CreatDefultGraphNodeData(tempNode);
                    graphSaveContainer.defualtNodeDatas.Add(nodeData);
                }
                else
                {
                    GraphNodeData nodeData = NodeDataFactory.CreatGraphNodeData(tempNode);
                    graphSaveContainer.normalNodeDatas.Add(nodeData);
                }
            }
        }

        private void SaveNodeLinkDatas(GraphSaveContainer graphSaveContainer)
        {
            List<Edge> linkedEdges = graphEditorView.edges.ToList();
            for (int i = 0; i < linkedEdges.Count; i++)
            {
                Edge edge = linkedEdges[i];
                BaseNode outputNode = edge.output.node as BaseNode;
                BaseNode inputNode = edge.input.node as BaseNode;
                GraphNodeLinkData linkData = new GraphNodeLinkData
                {
                    OriginNodeGUID = outputNode.GUID,
                    OriginNodeOutputPortName = edge.output.portName,
                    TargetNodeGUID = inputNode.GUID,
                };
                graphSaveContainer.graphNodeLinkDatas.Add(linkData);
            }
        }
        #endregion

        #region Private Functions - Load
        private void LoadNodeDatas(GraphSaveContainer graphSaveContainer)
        {
            graphSaveContainer.normalNodeDatas.ForEach(
                data =>
                {
                    var startNode = graphEditorView.nodes.Cast<BaseNode>().
                        First(curNode => curNode.NodeTypeStr == EditorNodeTypeString.START) as RootNode;
                    startNode.Reset(data);
                    loadedNodes.Add(startNode);
                }
            );

            foreach (var data in graphSaveContainer.defualtNodeDatas)
            {
                DefaultDialogNode defaultDialogNode = NodeManager.CreateNode<DefaultDialogNode>();
                defaultDialogNode.Reset(data);
                graphEditorView.AddElement(defaultDialogNode);
                loadedNodes.Add(defaultDialogNode);
            }
        }

        private void LoadNodeLinkDatas(GraphSaveContainer graphSaveContainer)
        {
            if (!graphEditorView.nodes.Any())
            {
                EditorUtility.DisplayDialog("Error", "Invalid node list !", "OK");
                return;
            }
            loadedNodes.ForEach(curNode =>
            {
                var connectionDatas = graphSaveContainer.graphNodeLinkDatas.
                    Where(curLinkData => curNode.GUID == curLinkData.OriginNodeGUID).ToList();
                if (connectionDatas.Any())
                {
                    for (int i = 0; i < connectionDatas.Count; i++)
                    {
                        var curLinkData = connectionDatas[i];
                        var targetNode = graphEditorView.nodes.Cast<BaseNode>().
                            First(targetNode => targetNode.GUID == curLinkData.TargetNodeGUID);
                        if (targetNode != null)
                        {
                            Edge linkEdge = curNode.outputContainer[i].Q<Port>().
                                ConnectTo(targetNode.inputContainer[0].Q<Port>());
                            graphEditorView.AddElement(linkEdge);
                        }
                    }
                }
            });
            loadedNodes.Clear();
        }
        #endregion
    }
}
