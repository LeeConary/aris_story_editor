using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Experimental.GraphView;
using System;
using UnityEngine.UIElements;
using System.Runtime.CompilerServices;

namespace ArisStudio.StoryEditor
{
    public class ElementManager
    {
        public static TextField CreateTextField(string value, string label = null, EventCallback<ChangeEvent<string>> stringValueChangedcallback = null)
        {
            TextField textField = new TextField()
            {
                value = value,
                label = label
            };
            textField.SetValueWithoutNotify(value);
            textField.RegisterValueChangedCallback(stringValueChangedcallback);
            return textField;
        }

        public static TextField CreateTextArea(string value = null, string label = null, EventCallback<ChangeEvent<string>> onValueChanged = null)
        {
            TextField textArea = CreateTextField(value, label, onValueChanged);
            textArea.multiline = true;
            return textArea;
        }
    }

    public class NodeManager
    {
        public static object CreateNode(Type type, string name = "Default")
        {
            BaseNode node = Activator.CreateInstance(type, name, type.Name) as BaseNode;
            node.Init();
            return node;
        }

        public static T CreateNode<T>(string name = "Default") where T : BaseNode
        {
            Type type = typeof(T);
            BaseNode node = Activator.CreateInstance(type, name, type.Name) as T;
            node.Init();
            return node as T;
        }

        public static EditorNodeType SetNodeType(string typeName)
        {
            switch (typeName)
            {
                case "RootNode": return EditorNodeType.START;
                case "DefaultDialogNode": return EditorNodeType.DEFAULT_DIALOG;
                default:
                    return EditorNodeType.UNKNOWN;
            }
        }
    }
}

