using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Experimental.GraphView;
using UnityEngine.UIElements;

namespace ArisStudio.StoryEditor
{
    public class BaseSerachWindow : ScriptableObject, ISearchWindowProvider
    {
        private MainGraphEditorView graphView;
        private Texture2D intentionIcon;

        public void Initialize(MainGraphEditorView graphView)
        {
            this.graphView = graphView;
            intentionIcon = new Texture2D(1, 1);
            intentionIcon.SetPixel(0, 0, new Color(0, 0, 0, 0));
            intentionIcon.Apply();
        }

        List<SearchTreeEntry> ISearchWindowProvider.CreateSearchTree(SearchWindowContext context)
        {
            var entries = new List<SearchTreeEntry>();
            entries.Add(new SearchTreeGroupEntry(new GUIContent("Create Nodes"), 0));

            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var type in assembly.GetTypes())
                {
                    if (type.IsClass && !type.IsAbstract && (type.IsSubclassOf(typeof(BaseNode)))
                        && type != typeof(RootNode))
                    {
                        string space = "   ";
                        entries.Add(new SearchTreeEntry(new GUIContent(space + type.Name, intentionIcon)) { level = 1, userData = type });
                    }
                }
            }

            return entries;
        }

        bool ISearchWindowProvider.OnSelectEntry(SearchTreeEntry searchTreeEntry, SearchWindowContext context)
        {
            var type = searchTreeEntry.userData as System.Type;
            var node = NodeManager.CreateNode(type, type.Name) as BaseNode;
            var pos = graphView.GetLocalMousePosition(context.screenMousePosition);

            node.SetPosition(new Rect(pos, GraphViewConsts.DEFAULT_NODE_SIZE));
            graphView.AddElement(node);
            return true;
        }
    }
}


