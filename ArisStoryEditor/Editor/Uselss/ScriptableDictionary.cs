using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create Scriptable Dictionary")]
public class ScriptableDictionary : ScriptableObject, ISerializationCallbackReceiver
{
    [SerializeField]
    List<string> keys = new List<string>();
    [SerializeField]
    List<UnityEngine.Object> arrayValues = new List<UnityEngine.Object>();

    Dictionary<string, UnityEngine.Object> dics = new Dictionary<string, UnityEngine.Object>();
    public void OnAfterDeserialize()
    {
        dics.Clear();
        int dicsCount = Math.Min(keys.Count, arrayValues.Count);
        for (int i = 0; i < dicsCount; i++)
        {
            dics.Add(keys[i], arrayValues[i]);
        }
    }

    public void OnBeforeSerialize()
    {
        foreach (var kvp in dics)
        {
            if (!keys.Contains(kvp.Key))
            {
                keys.Add(kvp.Key);
                arrayValues.Add(kvp.Value);
            }
            else
            {
                int idx = keys.IndexOf(kvp.Key);
                if (idx > arrayValues.Count)
                {
                    arrayValues.Add(kvp.Value);
                }
                else
                {
                    arrayValues[idx] = kvp.Value;
                }
            }
        }
    }

    public UnityEngine.Object this[string key]
    {
        get
        {
            UnityEngine.Object list = null;
            dics.TryGetValue(key, out list);
            return list;
        }
        set
        {
            dics[key] = value;
        }
    }
}
