using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace ArisStudio.StoryEditor
{
    public class MGraphViewEditorWindow : EditorWindow
    {
        #region Varibles
        public MainGraphEditorView EditorGraphView { get; private set; }
        #endregion

        [MenuItem("Aris Studio/Story Graph Editor")]
        public static void OpenWindow()
        {
            var window = GetWindow<MGraphViewEditorWindow>();
            window.minSize = new Vector2(500, 500);
            window.titleContent = new GUIContent("Aris Editor");
            window.Show();
        }

        private void OnEnable()
        {
            ConstructGraphView();
            InitModules();
            ConstructToolbar();
        }

        private void InitModules()
        {
            GraphIOModule.CreateInstance(EditorGraphView);
        }

        private void OnDisable()
        {

        }

        #region Private Functions
        void ConstructGraphView()
        {
            EditorGraphView = new MainGraphEditorView
            {
                name = "Story Graph Editor",
                viewContainingWindow = this,
            };

            EditorGraphView.StretchToParentSize();
            rootVisualElement.Add(EditorGraphView);
        }

        private void ConstructToolbar()
        {
            TextField textField = new TextField
            {
                value = "Default Assets",
            };
            textField.RegisterValueChangedCallback(str => textField.value = str.newValue);

            Toolbar toolbar = new Toolbar();
            Action saveAct = () => 
            {
                GraphIOModule.Instance.SaveGraph(textField.value);
            };
            Action loadAct = () => 
            {
                GraphIOModule.Instance.LoadGraph(textField.value + ".aris");
            };
            Action export = () => { };
            Action clearGraph = () =>
            {
                EditorGraphView.ClearAllNodesAndEdges();
            };
            Button saveBtn = new Button(saveAct) { text = GraphViewConsts.TOOLBAR_SAVE_BTN };
            Button loadBtn = new Button(loadAct) { text = GraphViewConsts.TOOLBAR_LOAD_BTN };
            Button exportBtn = new Button(export) { text = GraphViewConsts.TOOLBAR_EXPORT_SCRIPT };
            Button clearBtn = new Button(clearGraph) { text = GraphViewConsts.TOOLBAR_CLEAR_GRAPH };

            toolbar.Add(textField);
            toolbar.Add(saveBtn);
            toolbar.Add(loadBtn);
            toolbar.Add(exportBtn);
            toolbar.Add(clearBtn);

            rootVisualElement.Add(toolbar);
        }
        #endregion

    }
}

