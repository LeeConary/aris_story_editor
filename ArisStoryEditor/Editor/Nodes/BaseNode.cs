using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Experimental.GraphView;
using System.Runtime.CompilerServices;
using UnityEngine.UIElements;
using System.Linq;
using UnityEditor;
using System;

namespace ArisStudio.StoryEditor
{
    public class BaseNode : Node, IBaseNode
    {
        #region Varibles
        MainGraphEditorView _graphEditorView;
        MainGraphEditorView graphEditorView
        {
            get
            {
                if (_graphEditorView == null)
                {
                    var window = EditorWindow.GetWindow<MGraphViewEditorWindow>();
                    var view = window.EditorGraphView;
                    if (view != null) _graphEditorView = view;
                }
                return _graphEditorView;
            }
        }
        #endregion

        #region Public Varibles
        public string GUID { get; protected set; }
        public string Name { get; protected set; }
        public EditorNodeType NodeType { get; private set; }
        public string NodeTypeStr { get; private set; }

        #endregion
        public BaseNode(string name, EditorNodeType type = EditorNodeType.UNKNOWN)
        {
            title = name;
            this.name = name;
            Name = name;
            NodeType = type;
        }

        public BaseNode(string name, string nodeTypeStr)
        {
            title = name;
            this.name = name;
            Name = name;
            NodeTypeStr = nodeTypeStr;
            NodeType = NodeManager.SetNodeType(nodeTypeStr);
        }

        #region Port Manages
        public Port GenerateOutputPort(string portName = "Output", Type varType = null, Port.Capacity capacity = Port.Capacity.Single)
        {
            var port = GeneratePort(portName, Direction.Output, varType, capacity);
            outputContainer.Add(port);
            RefreshNodeStatus();
            return port;
        }

        public Port GenerateInputPort(string portName = "Input", Type varType = null, Port.Capacity capacity = Port.Capacity.Single)
        {
            var port = GeneratePort(portName, Direction.Input, varType, capacity);
            inputContainer.Add(port);
            RefreshNodeStatus();
            return port;
        }

        public void RemoveOutputPort(Port port)
        {
            RemoveConnectEdgeByNodeOutputPort(port);
            outputContainer.Remove(port);
            RefreshNodeStatus();
        }

        public void RemoveConnectEdgeByNodeOutputPort(Port port)
        {
            List<Edge> edges = graphEditorView.edges.ToList();
            var targetEdge = edges.Where(edge => edge.output.portName == port.portName
                && edge.output.node == this);
            if (targetEdge.Any())
            {
                List<Edge> connectedEdges = targetEdge.ToList();
                for (int i = 0; i < connectedEdges.Count; i++)
                {
                    Edge edge = connectedEdges[i];
                    edge.input.Disconnect(edge);
                    graphEditorView.RemoveElement(edge);
                }
                RefreshNodeStatus();
            }   
        }

        Port GeneratePort(string portName, Direction direction, Type varType = null, Port.Capacity capacity = Port.Capacity.Single)
        {
            if (varType == null) varType = typeof(int);
            var port = InstantiatePort(Orientation.Horizontal, direction, capacity, varType);
            port.portName = portName;
            return port;
        }
        #endregion

        #region Override Functions
        public virtual void Init()
        {
            GUID = Guid.NewGuid().ToString();
        }
        public virtual void Reset(GraphNodeData nodeData = null) 
        {
            if (nodeData != null)
            {
                this.GUID = nodeData.GUID;
                this.Name = nodeData.Name;
                this.SetPosition(nodeData.Position);
            }
        }
        public virtual GraphNodeData GetData()
        {
            GraphNodeData data = new GraphNodeData
            {
                GUID = this.GUID,
                Name = this.Name,
                NodeType = this.NodeType,
                NodeTypeStr = this.NodeTypeStr,
                Position = this.GetPosition()
            };
            return data;
        }
        #endregion

        #region Private Functions
        protected void RefreshNodeStatus()
        {
            RefreshExpandedState();
            RefreshPorts();
        }
        #endregion
    }
}

