using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace ArisStudio.StoryEditor
{
    public class RootNode : BaseNode
    {
        public RootNode(string name, EditorNodeType type) : base(name, type) { }

        public RootNode(string name, string typeStr) : base(name, typeStr) { }

        public override void Init()
        {
            base.Init();
            SetPosition(new Rect(GraphViewConsts.DEFAULT_NODE_POS, GraphViewConsts.DEFAULT_NODE_SIZE));
            GenerateOutputPort();
            capabilities -= Capabilities.Deletable;
            capabilities -= Capabilities.Movable;
        }
    }
}
