using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArisStudio.StoryEditor
{
    using System;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using UnityEngine.UIElements;
    public sealed class DefaultDialogNode : BaseNode
    {
        public string DialogContent { get; private set; }
        public DefaultDialogNode(string name, EditorNodeType type) : base(name, type) { }
        public DefaultDialogNode(string name, string typeStr) : base(name, typeStr) { }

        TextField nameTextField;
        TextField contentTextArea;

        public override void Init()
        {
            base.Init();
            GenerateInputPort("Previous");
            GenerateOutputPort("Next");

            Label defaultLabel = titleContainer.Q<Label>();
            if (defaultLabel != null)
                titleContainer.Remove(defaultLabel);

            AddTitleText();
            AddExtentContainer();
           
            RefreshNodeStatus();
        }
        public override void Reset(GraphNodeData nodeData = null)
        {
            base.Reset(nodeData);
            DefaultGraphNodeData defaultNodeData = nodeData as DefaultGraphNodeData;
            this.DialogContent = defaultNodeData.DialogContent;

            nameTextField.SetValueWithoutNotify(this.Name);
            contentTextArea.SetValueWithoutNotify(this.DialogContent);
        }
        public override GraphNodeData GetData()
        {
            GraphNodeData data = base.GetData();
            DefaultGraphNodeData defaultData = new DefaultGraphNodeData(data) 
            { 
                DialogContent = this.DialogContent,
            };
            return defaultData;
        }

        private void AddExtentContainer()
        {
            VisualElement customContainer = new VisualElement();
            customContainer.AddClasses(
                "aris_node__custom-container"
            );
            extensionContainer.Add(customContainer);
            AddMainContentText(customContainer);
        }

        private void AddMainContentText(VisualElement customContainer)
        {
            Foldout foldout = new Foldout()
            { 
                text = "Main Content",
            };

            contentTextArea = ElementManager.CreateTextArea(null, null, callback =>
            {
                TextField field = callback.target as TextField;
                field.value = callback.newValue;
                this.DialogContent = field.value;
            });
            contentTextArea.AddClasses(
                "aris_node__text-area"
            );
            foldout.Add(contentTextArea);
            customContainer.Add(foldout);
        }

        private void AddTitleText()
        {
            nameTextField = ElementManager.CreateTextField(this.Name, null, callback =>
            {
                TextField field = callback.target as TextField;
                field.value = callback.newValue;
                this.Name = field.value;
            });

            nameTextField.AddClasses(
                    "aris_node__text-field",
                    "aris_node__text-field__hidden",
                    "aris_node__filename-text-field"
                );
            titleContainer.Insert(0, nameTextField);
        }
    }
}

