
public interface IBaseNode
{
    public void Init();
    public void Reset(GraphNodeData nodeData);
    public GraphNodeData GetData();
}
