using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace ArisStudio.StoryEditor
{
    public class GraphViewConsts
    {
        public static readonly Vector2 DEFAULT_NODE_POS = Vector2.zero;
        public static readonly Vector2 DEFAULT_NODE_SIZE = new Vector2(200, 150);

        public static readonly string TOOLBAR_SAVE_BTN = "Save Graph";
        public static readonly string TOOLBAR_LOAD_BTN = "Load Graph";
        public static readonly string TOOLBAR_EXPORT_SCRIPT = "Export Script";
        public static readonly string TOOLBAR_CLEAR_GRAPH= "Clear All";
    }

    public class MainGraphEditorView : GraphView
    {
        public EditorWindow viewContainingWindow
        {
            private get; set;
        }

        public MainGraphEditorView()
        {
            #region Init Style And Operate Events
            this.AddStyleSheets(true, 
                "GraphViewStyles",
                "ArisNodeStyle",
                "StyleVaribles"
            );
            styleSheets.Add(Resources.Load<StyleSheet>("GraphViewStyles"));
            SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale * 2f);

            var grid = new GridBackground();
            Insert(0, grid);
            grid.StretchToParentSize();

            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());
            #endregion

            #region Create Start Node
            var baseNode = NodeManager.CreateNode<RootNode>("START");

            AddElement(baseNode);
            #endregion

            #region Initilize SearchWindow
            var searchWindowProvider = ScriptableObject.CreateInstance<BaseSerachWindow>();
            searchWindowProvider.Initialize(this);

            nodeCreationRequest += context =>
            {
                // Vector2 mousePosition = GetLocalMousePosition(context.screenMousePosition, true);
                SearchWindowContext searchContext = new SearchWindowContext(context.screenMousePosition);
                SearchWindow.Open(searchContext, searchWindowProvider);
            };
            #endregion
        }

        /// <summary>
        /// Get mouse postion from world pos to GUI local
        /// </summary>
        public Vector2 GetLocalMousePosition(Vector2 mousePosition, bool isSearchWindow = false)
        {
            Vector2 worldMousePosition = viewContainingWindow.rootVisualElement.ChangeCoordinatesTo(
                    viewContainingWindow.rootVisualElement.parent, mousePosition - viewContainingWindow.position.position
                    );
            Vector2 localMousePosition = contentViewContainer.WorldToLocal(worldMousePosition);
            return localMousePosition;
        }

        /// <summary>
        /// Allow Nodes to link
        /// </summary>
        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            List<Port> compatiblePorts = new List<Port>();
            ports.ForEach((port) =>
            {
                if (startPort != port && startPort.node != port.node)
                {
                    compatiblePorts.Add(port);
                }
            });
            return compatiblePorts;
        }

        public void ClearAllNodesAndEdges()
        {
            List<BaseNode> validNodes = nodes.Cast<BaseNode>().ToList();
            validNodes.ForEach(curNode =>
            {
                List<Edge> connectEdge = edges.Where(edge => edge.output.node == curNode).ToList();
                if (connectEdge.Any())
                {
                    connectEdge.ForEach(curEdge =>
                    {
                        curEdge.output.Disconnect(curEdge);
                        curEdge.input.Disconnect(curEdge);
                        RemoveElement(curEdge);
                    });
                }

                if (curNode.NodeTypeStr != EditorNodeTypeString.START)
                    RemoveElement(curNode);
            });
        }
    }
}

